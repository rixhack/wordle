import sys
f = open(sys.argv[1],'r') # All words
f2 = open(sys.argv[2],'r') # Common words (sorted)
f3 = open(sys.argv[3], 'r') # First word
allwords = f.readlines()
commons = f2.readlines()
possible = commons
palabra = f3.readlines()[0].replace('\n','')
print("Mi primera palabra es " + palabra + ", dime qué tal: ")
a = input()
while a != '22222':
    black = []
    yellow = {}
    green = {}
    for i in range(0,5):
        if a[i] == '2':
            green[i] = palabra[i]
        elif a[i] == '1':
            yellow[i] = palabra[i]
        else:
            if palabra[i] not in black:
                black.append(palabra[i])
    print(green)
    print(yellow)
    print(black)
    newPosible = []
    for w2 in possible:
        posible = True
        for k in green.keys():
            if w2[k] != green[k]:
                posible = False
                break
        if posible == False:
            continue
        for k in yellow.keys():
            if yellow[k] not in w2:
                posible = False
                break
            elif w2[k] == yellow[k]:
                posible = False
                break
            elif len(green) > 0 and len([x for x in green.values() if x == yellow[k]]) >= len([x for x in w2 if x == yellow[k]]):
                posible = False
                break
        if posible == False:
            continue
        for k in black:
            if k in w2 and len([x for x in green.values() if x == k]) != len([x for x in w2 if x == k]) and len([x for x in yellow.values() if x == k]) != len([x for x in w2 if x == k]):
                posible = False
                break
        if posible == False:
            continue
        newPosible.append(w2)
    newSorted = newPosible
    newSorted.sort()
    print(newSorted)
    possible = newPosible
    maxScore = -1
    for w1 in possible:
        score = 0
        for w2 in possible:
            if w1 == w2:
                continue
            black = []
            yellow = {}
            green = {}
            for i in range(0,5):
                if w1[i] == w2[i]:
                    green[i] = w1[i]
                elif w1[i] in w2:
                    if (len([x for x in green.values() if x == w1[i]]) < len([x for x in w2 if x == w1[i]])):
                        yellow[i] = w1[i]
                else:
                    if w1[i] not in black:
                        black.append(w1[i])
            aux = []            
            for y in yellow.keys():
                if len([x for x in green.values() if x == yellow[y]]) == len([x for x in w2 if x == yellow[y]]):
                    aux.append(y)
            for k in aux:
                del yellow[k]
            w2score = 3*len(green) + 2*len(yellow) + len(black)
            score = score + w2score

        if score > maxScore:
            print("Nueva mejor palabra: " + w1 + " con score = " + str(score))
            maxScore = score
            palabra = w1
        if len(possible) < 10:
            minIndex = len(commons)
            for p in possible:
                if commons.index(p) < minIndex:
                    palabra = p
                    minIndex = commons.index(p)
    print("Mi siguiente palabra es " + palabra + ", dime qué tal: ")
    a = input()            


